export const handleStatus = res => 
    res.ok ? res.json() : Promise.reject(res.statusText) // se a promise for realizada com êxito, retorna o JSON, se não, retorn erro

export const log = param => {
  console.log(param) // faz o console.log e retorna, para a continuidade da promise
  return param
}

export const timeoutPromise = (millisec, promise) => {
  const timeout = new Promise((resolve, reject) => {
    setTimeout(() => 
        reject(`Limite da operação excedido (limite: ${millisec})`), millisec)
    })

    return Promise.race([timeout, promise])
}

export const delay = millisec => data => 
    new Promise((resolve, reject) => setTimeout(() => resolve(data), millisec))

export const retry = (retries, millisec, fn) => 
    fn().catch(err => {
      console.log(err)
      return delay(millisec)().then(() => 
            retries > 1 
            ? retry(--retries, millisec, fn) 
            : Promise.reject(err))
    })
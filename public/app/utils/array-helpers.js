if (!Array.prototype.$flatMap) {
  Array.prototype.$flatMap = function(cb) {
    return this.map(cb).reduce((flatArray, array) => flatArray.concat(array), [])
  }
}